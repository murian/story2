package com.lab3.figures;

import java.awt.Component;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class TitlesFrame extends JFrame {
    public TitlesFrame() {
        this.initUI();
    }

    /*
    This method is initialize UI interface
     **/
    private void initUI() {
        this.setTitle("Кривые фигуры");
        this.setDefaultCloseOperation(3);
        this.add(new TitlesPanel(44));
        this.setSize(350, 350);
        this.setLocationRelativeTo(null);
    }

    /*
    * This is main method
    * **/

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                TitlesFrame ps = new TitlesFrame();
                ps.setVisible(true);
            }
        });
    }
}

