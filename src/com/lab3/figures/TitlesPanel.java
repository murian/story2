package com.lab3.figures;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import javax.swing.JPanel;
import javax.swing.Timer;

public class TitlesPanel extends JPanel implements ActionListener {
    private Graphics2D g2d;
    private Timer animation;
    private boolean is_done = true;
    private int start_angle = 0;
    private int shape;



    public TitlesPanel(int _shape) {
        this.shape = _shape;
        this.animation = new Timer(50, this);
        this.animation.setInitialDelay(60);
        this.animation.start();
    }

    /*
    * method repaint TitlesPanel
    * */
    public void actionPerformed(ActionEvent arg0) {
        if(this.is_done) {
            this.repaint();
        }

    }

     /*
    * Draw a shape objects
    * */

    private void doDrawing(Graphics g) {
        this.is_done = false;
        g2d = (Graphics2D)g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        Dimension size = this.getSize();
        Insets insets = this.getInsets();
        int w = size.width - insets.left - insets.right;
        int h = size.height - insets.top - insets.bottom;
        ShapeFactory shape = new ShapeFactory(this.shape);
        g2d.setStroke(shape.getStroke());
        g2d.setPaint(shape.getPaint());
        double angle = (double)(this.start_angle++);
        if(this.start_angle > 360) {
            this.start_angle = 0;
        }

        double dr = 90.0D / ((double)w / ((double)shape.getWidth() * 1.5D));

        for(int j = shape.getHeight(); j < h; j = (int)(j + shape.getHeight() * 1.5D)) {
            for(int i = shape.getWidth(); i < w; i = (int)(i + shape.getHeight() * 1.5D)) {
                angle = angle > 360.0D?0.0D:angle + dr;
                AffineTransform transform = new AffineTransform();
                transform.translate((double)i, (double)j);
                transform.rotate(Math.toRadians(angle));
                g2d.draw(transform.createTransformedShape(shape.getShape()));
            }
        }
        this.is_done = true;
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        this.doDrawing(g);
    }
}
